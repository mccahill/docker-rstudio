FROM  ubuntu:22.04

ENV TZ=America/New_York

RUN echo "rebuild"

# Core Tools
RUN apt-get update \
    && apt-get dist-upgrade -y \
    && DEBIAN_FRONTEND=noninteractive apt-get  install -y software-properties-common \
    && add-apt-repository universe 

RUN apt-get update \
    && DEBIAN_FRONTEND=noninteractive apt-get  install -y --no-install-recommends \
    vim-tiny \
    less \
    net-tools \
    inetutils-ping \
    curl \
    #git \
    telnet \
    nmap \
    socat \
    wget \
    sudo \
    sqlite3 \
    gdal-bin \
    libgdal-dev \
    build-essential \
    gdebi-core \
    locales \
    tzdata \
    gnupg2 \
    apt-utils \
    ssh \
    software-properties-common \
    libmagick++-dev \
    pandoc \
    supervisor \
    libopenblas-base \
    libcurl4-openssl-dev \
    libxml2-dev \
    libssl-dev \
    libfreetype6-dev \
    libudunits2-dev \
    libtiff-dev \
    libcairo2-dev \
    libxt-dev \
    libavfilter-dev \
    libopenmpi-dev \
    libblas-dev \
    liblapack-dev \
    libarpack2-dev \
    libglpk40 \
    software-properties-common \
    dirmngr \
    libgit2-dev 

#Install Git version 2.46
RUN add-apt-repository ppa:git-core/ppa \
    && apt-get update \
    && DEBIAN_FRONTEND=noninteractive apt-get install -y --no-install-recommends \
    git \
    && apt-get clean \
    && rm -rf /var/lib/apt/lists/*

RUN  mkdir -p /var/log/supervisor

#Commented this out since there is an error stating 
#"There can only be one CMD instruction in a dockerfile or build stage. Only the last on will have an effect."
#Seems you could combine the into a shell script and run that as your CMD instruction. Something like CMD ["./script.sh"]
#CMD ["/usr/bin/supervisord", "-n"]

# set the locale so RStudio doesn't complain about UTF-8 and set the timezome
RUN echo "en_US.UTF-8 UTF-8" >> /etc/locale.gen \
    && locale-gen en_US.utf8 \
    && /usr/sbin/update-locale LANG=en_US.UTF-8 \
	&& ln -fs /usr/share/zoneinfo/America/New_York /etc/localtime \
    && dpkg-reconfigure --frontend noninteractive tzdata

ENV LC_ALL=en_US.UTF-8
ENV LANG=en_US.UTF-8

ENV DEBIAN_FRONTEND=noninteractive 

# texLive
RUN wget https://mirrors.rit.edu/CTAN/systems/texlive/tlnet/install-tl-unx.tar.gz && \
     tar -zxf install-tl-unx.tar.gz  && \
     cd install-tl-* && \
     perl ./install-tl --no-interaction && \
     echo 'PATH=/usr/local/texlive/2023/bin/x86_64-linux:$PATH' >> /etc/bash.bashrc		

# https://cran.rstudio.com/bin/linux/ubuntu/
# Add the signing key (by Michael Rutter) for these repos
# To verify key, run gpg --show-keys /etc/apt/trusted.gpg.d/cran_ubuntu_key.asc  
# Fingerprint: E298A3A825C0D65DFD57CBB651716619E084DAB9
# add the R 4.0 repo from CRAN -- adjust 'focal' to 'groovy' or 'bionic' as needed
# Install R-Studio server latest 
RUN wget -qO- https://cloud.r-project.org/bin/linux/ubuntu/marutter_pubkey.asc | tee -a /etc/apt/trusted.gpg.d/cran_ubuntu_key.asc && \
    add-apt-repository "deb https://cloud.r-project.org/bin/linux/ubuntu jammy-cran40/" && \
    apt update -qq && \
    apt install --no-install-recommends -y \
        r-base \
        r-base-dev \
        littler && \
    ln -s /usr/lib/R/site-library/littler/examples/install.r /usr/local/bin/install.r && \
    ln -s /usr/lib/R/site-library/littler/examples/install2.r /usr/local/bin/install2.r && \
    ln -s /usr/lib/R/site-library/littler/examples/installGithub.r /usr/local/bin/installGithub.r && \
    ln -s /usr/lib/R/site-library/littler/examples/testInstalled.r /usr/local/bin/testInstalled.r && \
    install.r docopt && \
    rm -rf /tmp/downloaded_packages/ /tmp/*.rds

RUN wget http://security.ubuntu.com/ubuntu/pool/main/o/openssl/libssl3_3.0.2-0ubuntu1.17_amd64.deb \
    && DEBIAN_FRONTEND=noninteractive gdebi -n ./libssl3_3.0.2-0ubuntu1.17_amd64.deb \
    && rm ./libssl3_3.0.2-0ubuntu1.17_amd64.deb

RUN wget https://download2.rstudio.org/server/jammy/amd64/rstudio-server-2024.04.2-764-amd64.deb \
    && DEBIAN_FRONTEND=noninteractive gdebi --n rstudio-server-*.deb \
	&& rm rstudio-server-*.deb

# quatro - is bundled with rstudio, but seeing a weird edge case for some documents
RUN wget https://github.com/quarto-dev/quarto-cli/releases/download/v1.5.56/quarto-1.5.56-linux-amd64.deb \
    && DEBIAN_FRONTEND=noninteractive gdebi --n quarto-1.5.56-linux-amd64.deb \
    && rm quarto-1.5.56-linux-amd64.deb

ADD ./Rprofileconf /Rprofile_conf 
RUN mv /Rprofile_conf/Rprofile.site /usr/lib/R/etc/Rprofile.site

# R packages base stuff
RUN install2.r --error -s --deps TRUE \
    rmarkdown \
    tidyverse \
    tidymodels \
    shiny \
    shinyjs \
    shinythemes \
    shinydashboard \
    devtools \
    anyflights \
    arm \
    ash \
    babynames \
    BHH2 \
    credentials \
    datasauRus \
    DiagrammeR \
    flexdashboard \
    forecast \
    forecTheta \
    gert \
    gganimate \
    ggdendro \
    ggforce \
    ggformula \
    ggmap \
    ggraph \
    gh \
    gifski \
    gitcreds \
    googlesheets4 

# R packages H-Z
RUN install2.r --error -s --deps TRUE \
    here \
    highlight \
    Hmisc \
    hms \
    learnr \
    lintr \
    magic \
    mapproj \
    maps \
    #maptools CRAN page recommends sf or terra instead
    mosaic \
    mosaicCore \
    mosaicData \
    NHANES \
    openintro \
    palmerpenguins \
    profvis \
    r2d3 \
    rcmdcheck \
    renv \
    rjson \
    RJSONIO \
    robotstxt \
    robustbase \
    skimr \
    spiderbar \
    splines2 \
    tufte \
    raster

RUN Rscript -e "install.packages('spDataLarge', repos = 'https://geocompr.r-universe.dev')"

RUN install2.r --error -s --deps TRUE \
    sf \
    sp \
    #rgeos CRAN page recommends sf or terra instead
    #rgdal CRAN page recommends sf or terra instead 
    leaflet \
    spData \
    spdep \
    spatialreg

# needed for something below
RUN Rscript -e "install.packages('cmdstanr', repos = c('https://mc-stan.org/r-packages/', getOption('repos')))" 
RUN Rscript -e "install.packages('rstan', repos = 'https://cloud.r-project.org/', dependencies = TRUE)" 

# Install dependencies for subselect
RUN R -e "install.packages(c('ISwR', 'corpcor'), repos='https://cloud.r-project.org')"

# Subselect dependency no longer available from CRAN. Installing from archive.
RUN wget https://cran.r-project.org/src/contrib/Archive/subselect/subselect_0.15.5.tar.gz \
    && R CMD INSTALL subselect_0.15.5.tar.gz \
    && rm subselect_0.15.5.tar.gz

# R packages - other
RUN install2.r --error -s --deps TRUE \
    bayesplot \
    cowplot \
    downlit \
    DT \
    GGally \
    miniUI \
    nycflights13 \
    ragg \
    RcppParallel \
    repurrrsive \
    rsconnect \
    stopwords \
    fs \
    janitor \
    ggridges \
    ggthemes \
    igraph \
    BAS  \
    airports \
    bench \
    betareg \
    BH \
    BHH2 \
    bitops \
    blogdown \
    bookdown \
    car \
    carData \
    caret \
    caTools \
    cherryblossom \
    chron \
    config \
    conquer \
    corrplot \
    credentials \
    CVST \
    cyclocomp 

RUN installGithub.r \
    rstudio/gradethis \
    rstudio-education/dsbox

# Config files
ADD ./biocInstalls /biocInstalls
RUN R CMD BATCH /biocInstalls/extras.R
RUN rm /extras.Rout 

# Install the archived loe package dependency as it was removed from CRAN
RUN wget https://cran.r-project.org/src/contrib/Archive/loe/loe_1.1.tar.gz \
    && R CMD INSTALL loe_1.1.tar.gz \
    && rm loe_1.1.tar.gz

RUN install2.r --error -s --deps TRUE \
    datasauRus \
    ddalpha \
    deldir \
    dendextend \
    DEoptimR \
    devtools \
    DiagrammeR \
    DiceDesign \
    dichromat \
    diffobj \
    dimRed \
    distributional \
    doMC \
    doParallel \
    dotCall64 \
    downloader \
    drat \
    DRR \
    dslabs \
    effects \
    #enc \
    evd \
    expm \
    fda \
    fds \
    fields \
    flexdashboard \
    flexmix \
    FNN \
    fontBitstreamVera \
    fontLiberation \
    fontquiver \
    forecast \
    forecTheta \
    forge \
    formatR \
    fracdiff \
    future.apply \
    gcookbook \
    gdata \
    geometry \
    geosphere \
    gert \
    gganimate \
    ggdendro \
    ggforce \
    ggformula \
    ggmap \
    ggraph \
    gh \
    gitcreds \
    gmodels \
    googlesheets4 \
    gower \
    GPfit \
    gplots \
    graphlayouts \
    greta \
    gridSVG \
    hdrcde \
    here \
    highlight \
    Hmisc \
    hms \
    HSAUR3 \
    htmlTable \
    import \
    ini \
    jpeg \
    keras \
    kernlab \
    ks \
    labelled \
    latticeExtra \
    leafem \
    leaflet \
    leaflet.providers \
    leafsync \
    LearnBayes \
    learnr \
    linprog \
    lintr \
    lmtest \
    locfit \
    lwgeom \
    magic \
    mapproj \
    maps \
    #maptools CRAN page recommends sf or terra instead
    MatrixModels \
    matrixStats \
    mclust \
    mice \
    microbenchmark \
    MLmetrics \
    mnormt \
    ModelMetrics \
    mosaic \
    mosaicCore \
    mosaicData \
    msm \
    multcomp \
    multicool \
    NHANES \
    openintro \
    openxlsx \
    palmerpenguins \
    pbkrtest \
    pcaPP \
    PKI \
    plotROC \
    pls \
    polspline \
    polyclip \
    profmem \
    profvis \
    pryr \
    psych \
    quantmod \
    quantreg \
    R.cache \
    R.methodsS3 \
    R.oo \
    R.rsp \
    R.utils \
    r2d3 \
    rainbow \
    raster \
    rcmdcheck \
    RcppProgress \
    RcppRoll \
    RCurl \
    remotes \
    renv \
    repr \
    reticulate \
    rgexf \
    RgoogleMaps \
    rio \
    rjson \
    RJSONIO \
    rms

RUN install2.r --error -s --deps TRUE \
    robotstxt \
    robustbase \
    ROCR \
    Rook \
    rticles \
    RUnit \
    rversions \
    RWiener \
    sandwich \
    scatterplot3d \
    seasonal \
    selectr \
    servr \
    sessioninfo \
    sf \
    sfsmisc \
    skimr \
    sp \
    spam \
    sparklyr \
    SparseM \
    spData \
    spdep \
    spiderbar \
    splines2 \
    stars \
    statmod \
    stringdist \
    styler \
    tensorflow \
    tfruns \
    TH.data \
    tmap \
    tmaptools \
    tmvnsim \
    tseries \
    TTR \
    udunits2  \
    ape \
    bridgesampling \
    Brobdingnag \
    colourpicker \
    corpcor \
    covr \
    cubature \
    dotwhisker \
    downlit \
    DT \
    dygraphs \
    emmeans  \
    gamm4 \
    GGally \
    glmbb \
    inline \
    ISOcodes \
    loo \
    MCMCglmm \
    miniUI \
    nleqslv \
    optimx \
    plogr \
    projpred \
    rngtools \
    rstantools \
    StanHeaders \
    bindrcpp \
    threejs \
    pairwiseCI  \
    tidybayes \
    tidygraph \
    tidypredict \
    tidyr \
    tidyselect \
    tidytext \
    tidytuesdayR \
    unvotes

# the maintainer removed this package from CRAN, but it is still available in the archives
RUN DEBIAN_FRONTEND=noninteractive wget https://cran.r-project.org/src/contrib/Archive/freetypeharfbuzz/freetypeharfbuzz_0.2.6.tar.gz \
    && R CMD INSTALL freetypeharfbuzz_0.2.6.tar.gz \
    && rm freetypeharfbuzz_0.2.6.tar.gz

# the default packages for everyone running R
RUN echo "" >> /etc/R/Rprofile.site && \
    echo "# add the downloader package to the default libraries" >> /etc/R/Rprofile.site && \
    echo ".First <- function(){" >> /etc/R/Rprofile.site && \
    echo "library(downloader)" >> /etc/R/Rprofile.site && \
    echo "library(knitr)" >> /etc/R/Rprofile.site && \
    echo "library(rmarkdown)" >> /etc/R/Rprofile.site && \
    echo "library(ggplot2)" >> /etc/R/Rprofile.site && \
    echo "library(googlesheets4)" >> /etc/R/Rprofile.site && \
    echo "library(openintro)" >> /etc/R/Rprofile.site && \
    echo "library(GGally)" >> /etc/R/Rprofile.site && \
    echo "library(babynames)" >> /etc/R/Rprofile.site && \
    echo "}" >> /etc/R/Rprofile.site  && \
    echo "" >> /etc/R/Rprofile.site

RUN adduser --disabled-password --gecos "" --ingroup users guest 

RUN mkdir -p /rstudio-server-instance/supervisord/logs/ && \
    chown -R guest /rstudio-server-instance/ && \
    chmod -R 755 /rstudio-server-instance/

# Config files
RUN mkdir -p /r-studio-configs
ADD ./conf /r-studio-configs 
RUN mkdir -p /etc/rstudio && \
    cd /r-studio-configs && \
    cp rserver.conf /etc/rstudio/ && \
    cp rsession.conf /etc/rstudio/ && \
    cp database.conf /etc/rstudio/ && \
    chown guest /etc/rstudio/* && \
    chown guest /var/log/supervisor

RUN Rscript -e "tinytex::install_tinytex()"
RUN Rscript -e 'install.packages("fivethirtyeight")'
RUN install2.r --error -s -r "https://fivethirtyeightdata.github.io/drat/" fivethirtyeightdata
RUN echo "PATH=/usr/local/texlive/2023/bin/x86_64-linux:$PATH" >> /etc/R/Renviron.site 

# set the glorious TZ env variable site-wide for R contexts
RUN echo 'TZ="America/New_York"' >> /etc/R/Renviron.site 

#########
#
# if you need additional tools/libraries, add them here.
# also, note that we use supervisord to launch both the password
# initialize script and the RStudio server. If you want to run other processes
# add these to the supervisord.conf file
#
#########

RUN apt-get autoremove -y \
    && apt-get clean

# Expose the necessary port
EXPOSE 8787 

RUN echo 'rebuild'

RUN cp /r-studio-configs/supervisord.conf /etc/supervisor/supervisord.conf

# Clean up a dangling session-server-rpc.socket
RUN rm -f /var/run/rstudio-server/rstudio-rserver/session-server-rpc.socket

USER guest

CMD ["/usr/bin/supervisord", "-c", "/etc/supervisor/supervisord.conf"]

